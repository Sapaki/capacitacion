<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Capacitacion' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'IqpqMzwI9ZzIwxf3MHaOm8mLFLfGqMsewOWeODoed7CCGWKB5Nch9CF6RhUw3VS4' );
define( 'SECURE_AUTH_KEY',  'f2CJDrTqaRRuvSVY11CavJ5HBBHfP08Lv4KFeuAhh78PhAofbTH27TrNDDV6SwqQ' );
define( 'LOGGED_IN_KEY',    'Enjhp0RKp9I2IkBlgIhIqL7ZwDZyqHOOgpL4oImAtxRjT8A6yW9hMWyL9W85Vu0k' );
define( 'NONCE_KEY',        'ua5NJ5I6yzNC8QFA05Q8Ec3Gr7hQk9rcXqzBsQ97ofeGZsUg3aCnwD0lEWGYH3Pb' );
define( 'AUTH_SALT',        'K3QJNEozEyomqeBKpOQjVKDSyzCPb9SeiY8XAxsoPcn0wAQ1Jw5RaFUyuAfSdNIQ' );
define( 'SECURE_AUTH_SALT', '2rneHD4QxbYjBOVJBQaScpNyNWQeroPLLMLKbPWBMU3wsDrRlDDBFs06rEtQBtNv' );
define( 'LOGGED_IN_SALT',   'XO2sJVohgBmQCZDMFrP4foaCTyrjJpaJ8kdSKTDdSxi0tds4DLAxusTpoDyGntms' );
define( 'NONCE_SALT',       'a7N5BkkSLVJFT9YuoHKIVgZZ0366BL02YHY9dlT4TYAEUdkvnmBxl7NzIFnmmP3Y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
